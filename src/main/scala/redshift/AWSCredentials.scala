package redshift

import com.typesafe.config.ConfigFactory

object AWSCredentials {

  def getAccessKey() : String ={
    val config =  ConfigFactory.load("application.conf")
    config.getString("awsAccessKey")
  }

  def getSecretKey() : String ={
    val config =  ConfigFactory.load("application.conf")
    config.getString("awsSecretKey")
  }

  def getBucketName() : String ={
    val config =  ConfigFactory.load("application.conf")
    config.getString("bucketName")
  }

}
