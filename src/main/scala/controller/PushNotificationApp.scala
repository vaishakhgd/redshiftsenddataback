package controller

import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONObject

object PushNotificationApp extends App{

  val client = HttpClientBuilder.create.build
  val post = new HttpPost("https://fcm.googleapis.com/fcm/send")
  post.setHeader("Content-type", "application/json")
  //Below is server token (find it in Firebase-console of your app ) for
  //post.setHeader("Authorization", "key=AAAAX1CdlS8:APA91bF0ijaN4TfelwA13FDKIK4TWm5zWx7YkCds90Hexu6RInf27CD9M9nRiCt7ZQZeXI69j1O3J5tMsTzHt3b0Vzr7aAVB0akJ_o2PesVrqLkoe-sHJPbSeW9HlwmmqmIpzwtLGTFo")
  // post.setHeader("Authorization","key=AAAAX1CdlS8:APA91bF0ijaN4TfelwA13FDKIK4TWm5zWx7YkCds90Hexu6RInf27CD9M9nRiCt7ZQZeXI69j1O3J5tMsTzHt3b0Vzr7aAVB0akJ_o2PesVrqLkoe-sHJPbSeW9HlwmmqmIpzwtLGTFo")
  var k1 = "AAAAX1CdlS8:APA91bF0ijaN4TfelwA13FDKIK4TWm5zWx7YkCds90Hexu6RInf27CD9M9nRiCt7ZQZeXI69j1O3J5tMsTzHt3b0Vzr7aAVB0akJ_o2PesVrqLkoe-sHJPbSeW9HlwmmqmIpzwtLGTFo"
  //AIzaSyCzUGj0P92hxG5sCPJRrcCS8zdHcqe1X88
  post.setHeader("Authorization","key="+k1)
  val message = new JSONObject;
  var deviceToken = "fIaYhLQIqxM:APA91bFn2tPthe7TkbH3tCi3vwbO8xeZl8c3aXhWQzmijf-yDc7rDXnpAEXVdpR6a33aKioZwxLimRpDSBVEZcYz0YUDbim7_IodPlaD_pPMtyA90yPevZyJssgrs38p38ekcFGScPrr"
  deviceToken = "fIaYhLQIqxM:APA91bEl8XM-yoT00DyqAuG9iM2SHeWm6ar5NKdfGwcRf00OaT92_fc0qgVMLesELfVpi6VO98BIP6vMc577nbXZYuw7r6oDs0kabECKvfh4jwHCPD7wMLNQDmrVXCpV2yjcR_fS204_"

  message.put("to", deviceToken)
  message.put("priority", "high")

  val notification = new JSONObject;
  notification.put("title", "Java")
  notification.put("body", "Lo Tigane")

  message.put("notification", notification)

  post.setEntity(new StringEntity(message.toString, "UTF-8"))
  val response = client.execute(post)
  System.out.println(response)
  System.out.println(message)

}
