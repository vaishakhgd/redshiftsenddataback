package controller

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.Done
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.{ExceptionHandler, RejectionHandler, Route}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes
import akka.stream.ActorMaterializer
import database.{CreateTable, RedShiftPOC}
import database.orm.{CreateTableDetails, GetLatLang, getSonika}
import sonika.GetDynamoDB
import spray.json.{DefaultJsonProtocol, RootJsonFormat}
import ch.megard.akka.http.cors.scaladsl.CorsDirectives._
import ch.megard.akka.http.cors.scaladsl.settings.CorsSettings

import scala.util.{Failure, Success}
// for JSON serialization/deserialization following dependency is required:
// "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.7"
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._

import scala.io.StdIn

import scala.concurrent.Future

import spray.json.RootJsonFormat
import spray.json.DefaultJsonProtocol._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

import akka.http.scaladsl.server.Directives._
import akka.actor.typed.scaladsl.adapter._
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.model._
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.RootJsonFormat
import spray.json.DefaultJsonProtocol._



import akka.http.scaladsl.server.{ Directives, Route }





object SprayJsonExample2 {


  implicit val system = ActorSystem(Behaviors.empty, "SprayExample")
  implicit val executionContext = system.executionContext

  import DefaultJsonProtocol._

  implicit val orderFormat = jsonFormat4(GetLatLang)

  import DefaultJsonProtocol._

  implicit val orderFormat3 = jsonFormat3(getSonika)

  var getd = new GetDynamoDB()


  private val cors2 = new CORSHandler {}

  def main(args: Array[String]): Unit = {


    val settings = CorsSettings
    settings.defaultSettings.withAllowGenericHttpRequests(true)

    val route: Route =


    /*  concat(

        options {
          cors.corsHandler(complete(StatusCodes.OK))
        } ~

        post{
          path("getLatLang"){ //CURRENT WORKING ONE
            entity(as[GetLatLang]){
              tableDetails =>
                val latLngList = RedShiftPOC.latLngCoordinatesFour(latitude = tableDetails.latitude,longitude = tableDetails.longitude )
                val latlng = RedShiftPOC.getLatLangSql(latLngList,tableDetails.devicetoken)

               // val saved  = CreateTable.createTable(latlng.latitude,latlng.longitude,tableDetails)
                cors.corsHandler(complete(tableDetails.toString))
            }
          }




          path("getsonika"){



            //  getd.compWithLibmagic()
            entity(as[getSonika]) {

              details =>

                  println(details.name)
              cors.corsHandler(complete("nakkan"))

            }
           // cors.corsHandler(complete("nakkan"))
          }

        }


      )*/ {

      import ch.megard.akka.http.cors.scaladsl.CorsDirectives._

      // Your CORS settings are loaded from `application.conf`

      // Your rejection handler
      val rejectionHandler = corsRejectionHandler.withFallback(RejectionHandler.default)

      // Your exception handler
      val exceptionHandler = ExceptionHandler { case e: NoSuchElementException =>
        complete(StatusCodes.NotFound -> e.getMessage)
      }

      // Combining the two handlers only for convenience
      val handleErrors = handleRejections(rejectionHandler) & handleExceptions(exceptionHandler)

      // Note how rejections and exceptions are handled *before* the CORS directive (in the inner route).
      // This is required to have the correct CORS headers in the response even when an error occurs.
      // format: off
      handleErrors {
        cors() {
          handleErrors {
            path("ping") {
              complete("pong")
            } ~
              path("pong") {
                failWith(new NoSuchElementException("pong not found, try with ping"))
              }

            path("getsonika"){

              entity(as[getSonika]) {

                details =>


                var returnedSentimentString =   getd.compWithLibmagic(review = details.review,rating = details.rating)

                  println(details.name)
                  cors2.corsHandler(complete(returnedSentimentString))

              }
            }

          }
        }
      }
      // format: on
    }

    // val bindingFuture = Http().newServerAt("0.0.0.0", 8080).bind(route)

    Http().newServerAt("0.0.0.0", 8080).bind(route)
    //  Http().bindAndHandle(MainRouter.routes, "0.0.0.0",  8080)
    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")


  }
}
