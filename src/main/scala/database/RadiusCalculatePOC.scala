package database

object RadiusCalculatePOC {

  def getRadius(latitude1 :Double,longitude1 : Double, latitude2 : Double,longitude2 : Double) ={


    var  latitude1Rad = Math.toRadians(latitude1);
    var longitude1Rad = Math.toRadians(longitude1);
    var latitude2Rad = Math.toRadians(latitude2);
    var  longitude2Rad = Math.toRadians(longitude2);

    // Haversine formula
    var dlon = longitude2Rad - longitude1Rad;
    var dlat = latitude2Rad - latitude1Rad;
    var a = Math.pow(Math.sin(dlat / 2), 2)+ Math.cos(latitude1Rad) * Math.cos(latitude2Rad)* Math.pow(Math.sin(dlon / 2),2);

    var c = 2 * Math.asin(Math.sqrt(a));

    // Radius of earth in kilometers. Use 3956
    // for miles
    var r = 6371;

    // calculate the result  in mtrs
    Math.ceil(Math.abs(c*r*1000)).toLong
  }

}

/*
SELECT
    *,
    (
        3959
        * acos(
            cos( radians(70) )
            * cos( radians( latitude ) )
            * cos( radians( longitude ) - radians(79) )
            + sin( radians(70) )
            * sin( radians( latitude ) )
        )
    ) AS distance
FROM latIS70longIS79
Where distance < 1000 AND isvalid like '%%'
LIMIT (20)
OFFSET(0);
Hi, Returns query in kms
*/

