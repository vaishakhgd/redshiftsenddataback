package database

import com.typesafe.config.ConfigFactory
import java.sql._
import java.util.Properties

import com.google.gson.Gson
import pushnotification.PushNotification
import slick.jdbc.JdbcProfile

import scala.util.{Failure, Success}
//import slick.jdbc.JdbcBackend.Database


import database.orm.CreateTableDetails

import scala.concurrent.Future

import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global



object CreateTable {

  val config = ConfigFactory.load("application.conf")

  val dbUrl = config.getString("db.default.url")
  val MasterUsername = config.getString("db.default.username")
  val MasterUserPassword = config.getString("db.default.password")
  val driver = config.getString("db.default.driver")


  def getTableName(latitude: Int, longitude: Int) = {
    var latString = ""
    var longString = ""
    if (latitude < 0) {
      latString = "MINUS" + Math.abs(latitude).toString
    }
    else {
      latString = Math.abs(latitude).toString
    }
    if (longitude < 0) {
      longString = "MINUS" + Math.abs(longitude).toString
    }
    else {
      longString = Math.abs(longitude).toString
    }

    val s = "latIS" + latString + "longIS" + longString
    s
  }

  def getAttributes() = {

    val p =
      """(   id INT  GENERATED BY DEFAULT AS IDENTITY,
         phoneNo VARCHAR(100) NOT NULL,
         countryCode VARCHAR(100) NOT NULL,
         latitude DOUBLE PRECISION NOT NULL,
         longitude DOUBLE PRECISION NOT NULL,
         groupname VARCHAR(100) NOT NULL,
         detailsjson  TEXT NOT NULL,
         groupId VARCHAR(MAX) NOT NULL,
         deviceToken VARCHAR(MAX) NOT NULL,
         isValid VARCHAR(1000) NOT NULL,
          PRIMARY KEY (id,latitude,longitude,countryCode,phoneNo,groupId)
          );
    """
    //id must be removed from primary key in prod db
    p
  }

  def insertTable2()={
    val db2: Database = Database.forConfig("my_redshift")


  }

  def insertIntoTable(tablename : String,createTableDetails: CreateTableDetails, conn : Connection) ={
    try{
      Class.forName(driver)
      val props = new Properties()
      props.setProperty("user", MasterUsername)
      props.setProperty("password", MasterUserPassword)
      var conn2 =
        DriverManager.getConnection(dbUrl, props)
      var stmt = conn2.createStatement
      //('phno12','cccode',4,5,'gname','djson','gid','dtoken','isvalid')
      var sql = ""
      println("TABLENAME IS  "+ tablename)

     // sql =  " INSERT INTO " +tablename + " (phoneno,countrycode,latitude,longitude,groupname,detailsjson,groupid,devicetoken,isvalid) " +
     // " VALUES "+ " ( " + createTableDetails.phoneno+" , "+ createTableDetails.countrycode+","+createTableDetails.latitude+","+createTableDetails.longitude+","+createTableDetails.groupname+","+createTableDetails.detailsjson+","+createTableDetails.groupid+","+createTableDetails.devicetoken+","+createTableDetails.isvalid+");";

    val p =   " INSERT INTO " +tablename + "   (phoneno,countrycode,latitude,longitude,groupname,detailsjson,groupid,devicetoken,isvalid)  VALUES (?,?,?,?,?,?,?,?,?); "

      val preparedStmt: PreparedStatement = conn2.prepareStatement(p)



      preparedStmt.setString (1, createTableDetails.phoneno)
      preparedStmt.setString (2, createTableDetails.countrycode)
      preparedStmt.setDouble (3, createTableDetails.latitude)
      preparedStmt.setDouble (4, createTableDetails.longitude)

      preparedStmt.setString (5, createTableDetails.groupname)
      preparedStmt.setString (6, createTableDetails.detailsjson)
      preparedStmt.setString (7, createTableDetails.groupid)
      preparedStmt.setString (8, createTableDetails.devicetoken)
      preparedStmt.setString (9, createTableDetails.isvalid)

      preparedStmt.executeUpdate()







     // val rs = stmt.execute(sql)
    }
    catch {
      case e : Exception => e.printStackTrace()
    }
  }


  def getFromTable(latitude : Int,longitude : Int,devicetoken2 : String) = {

    try {
      Class.forName(driver)
      val props = new Properties()
      props.setProperty("user", MasterUsername)
      props.setProperty("password", MasterUserPassword)
      var conn =
        DriverManager.getConnection(dbUrl, props)
      var stmt = conn.createStatement

      /*

      SELECT
    *,
    (
        3959
        * acos(
            cos( radians(70) )
            * cos( radians( latitude ) )
            * cos( radians( longitude ) - radians(79) )
            + sin( radians(70) )
            * sin( radians( latitude ) )
        )
    ) AS distance
FROM latIS70longIS79
Where distance < 1000 AND isvalid like '%%'
LIMIT (20)
OFFSET(0);

      * */


      var sql = "  SELECT *,(3959*acos(cos(radians(" + latitude.toString +"))* cos(radians(latitude))* cos(radians(longitude) - radians( " + longitude.toString +" )) + sin(radians(" + latitude.toString+" ))* sin(radians( latitude )))) AS distance FROM   " + getTableName(latitude, longitude = longitude) + " " + " Where distance < 20000 AND isvalid like '%%' LIMIT (20) OFFSET(0)"

      val resultSet = stmt.executeQuery(sql)
      println("SQL IS "+sql)
      while ( resultSet.next() ) {
        val detailsjson = resultSet.getString("detailsjson")
        val distance = resultSet.getString("distance")
        val latitudeSql = resultSet.getString("latitude")
        val longitudeSql = resultSet.getString("longitude")
        val groupid = resultSet.getString("groupid")
        val groupname = resultSet.getString("groupname")
        val isvalid = resultSet.getString("isvalid")
        val devicetoken = resultSet.getString("devicetoken")
        val phoneno = resultSet.getString("phoneno")
        val countrycode = resultSet.getString("countrycode")


        var p =  CreateTableDetails(phoneno = phoneno,countrycode =countrycode,
                              latitude = latitude ,longitude=longitude ,
                              groupname=groupname ,detailsjson=detailsjson ,
                              groupid=groupid ,devicetoken=devicetoken ,isvalid=isvalid )


        var gson  = new Gson
        var t = gson.toJson(p)

        var pushNotification = new PushNotification
        pushNotification.pushNotification(deviceId = devicetoken2,message1 = t)

        println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
        println(detailsjson)
        println(distance)
        println(devicetoken)
        println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
      }
    }
    catch {
      case e: Exception => e.printStackTrace()
    }
  }
}
