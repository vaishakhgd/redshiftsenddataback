package database.orm

case class CreateTableDetails(phoneno : String,countrycode : String,
                              latitude : Double,longitude : Double,
                              groupname : String,detailsjson : String,
                              groupid : String,devicetoken : String,isvalid : String)


final case class GetLatLang(
                                    latitude : Double,longitude : Double,

                                    devicetoken : String, distance : Double)

final case class  getSonika(name : String,rating : String,review : String)


