package pushnotification

import com.google.gson.JsonObject
import org.apache.http.impl.client.HttpClientBuilder
import org.json.JSONObject

class PushNotification {

  def pushNotification(deviceId : String, message1 : String) = {
    import org.apache.http.client.methods.HttpPost
    import org.apache.http.entity.StringEntity

    val client = HttpClientBuilder.create.build
    val post = new HttpPost("https://fcm.googleapis.com/fcm/send")
    post.setHeader("Content-type", "application/json")
    //Below is server token (find it in Firebase-console of your app ) for
    //post.setHeader("Authorization", "key=AAAAX1CdlS8:APA91bF0ijaN4TfelwA13FDKIK4TWm5zWx7YkCds90Hexu6RInf27CD9M9nRiCt7ZQZeXI69j1O3J5tMsTzHt3b0Vzr7aAVB0akJ_o2PesVrqLkoe-sHJPbSeW9HlwmmqmIpzwtLGTFo")
   // post.setHeader("Authorization","key=AAAAX1CdlS8:APA91bF0ijaN4TfelwA13FDKIK4TWm5zWx7YkCds90Hexu6RInf27CD9M9nRiCt7ZQZeXI69j1O3J5tMsTzHt3b0Vzr7aAVB0akJ_o2PesVrqLkoe-sHJPbSeW9HlwmmqmIpzwtLGTFo")
    var k1 = "AAAAX1CdlS8:APA91bF0ijaN4TfelwA13FDKIK4TWm5zWx7YkCds90Hexu6RInf27CD9M9nRiCt7ZQZeXI69j1O3J5tMsTzHt3b0Vzr7aAVB0akJ_o2PesVrqLkoe-sHJPbSeW9HlwmmqmIpzwtLGTFo"
    //AIzaSyCzUGj0P92hxG5sCPJRrcCS8zdHcqe1X88
    post.setHeader("Authorization","key="+k1)
    val message = new JSONObject;
    var deviceToken = "fIaYhLQIqxM:APA91bFn2tPthe7TkbH3tCi3vwbO8xeZl8c3aXhWQzmijf-yDc7rDXnpAEXVdpR6a33aKioZwxLimRpDSBVEZcYz0YUDbim7_IodPlaD_pPMtyA90yPevZyJssgrs38p38ekcFGScPrr"
    deviceToken = "fIaYhLQIqxM:APA91bHxsw4fp3nFpBewaDGyW4C1FzO5JFMrlfgZpme5ruqeb08-OOIcJP9EUL8rnB1tX572LtKXTNTWjYBLRuNyRnL9RUI4ZW5EYS_I-kgFi9wNyxBwW2cengKTsYE82WH8DsuaWOhe"
    deviceToken = deviceId
    message.put("to", deviceToken)
    message.put("priority", "high")

    val notification = new JSONObject;
    notification.put("title", "Java")
    notification.put("body", message1)

    message.put("notification", notification)

    post.setEntity(new StringEntity(message.toString, "UTF-8"))
    val response = client.execute(post)
    System.out.println(response)
    System.out.println(message)

  }


}
