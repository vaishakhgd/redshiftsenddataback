

import org.apache.spark.sql.SparkSession
import org.apache.spark.{SparkConf, SparkContext}

object SparkSessionObject {

  var sparkSession : SparkSession = null

  def getSparkSession() : SparkSession ={
    this.sparkSession
  }

  def setSparkSession(sparkSession: SparkSession) ={
    this.sparkSession = sparkSession

  }

}